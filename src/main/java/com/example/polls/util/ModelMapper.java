package com.example.polls.util;

import com.example.polls.model.Poll;
import com.example.polls.model.User;
import com.example.polls.payload.ChoiceResponse;
import com.example.polls.payload.PollResponse;
import com.example.polls.payload.UserSummary;

import java.time.Instant;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class ModelMapper {

    public static PollResponse mapPollToPollResponse(Poll poll,
                                                     Map<Long, Long> choiceVotesMap,
                                                     User creator,
                                                     Long userVote) {

        PollResponse pollResponse = new PollResponse();
        pollResponse.setId(poll.getId());
        pollResponse.setQuestion(poll.getQuestion());
        pollResponse.setCreationDateTime(poll.getCreatedAt());
        pollResponse.setExpirationDateTime(poll.getExpirationDateTime());
        pollResponse.setExpired(poll.getExpirationDateTime().isBefore(Instant.now()));

        List<ChoiceResponse> choiceResponses = poll.getChoices()
                .stream()
                .map(choice -> {
                    ChoiceResponse choiceResponse = new ChoiceResponse();
                    choiceResponse.setId(choice.getId());
                    choiceResponse.setText(choice.getText());

                    choiceResponse.setVoteCount(choiceVotesMap.containsKey(choice.getId()) ? choiceVotesMap.get(choice.getId()) : 0);
                    return choiceResponse;
                })
                .collect(Collectors.toList());

        pollResponse.setChoices(choiceResponses);
        UserSummary creatorSummary = new UserSummary(
                creator.getId(),
                creator.getUsername(),
                creator.getName()
        );

        if (userVote != null) {
            pollResponse.setSelectedChoice(userVote);
        }

        long totalVotes = pollResponse.getChoices()
                .stream()
                .mapToLong(ChoiceResponse::getVoteCount)
                .sum();

        pollResponse.setTotalVotes(totalVotes);

        return pollResponse;
    }
}
